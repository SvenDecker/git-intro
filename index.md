Hallo.

# Welcome to the SooperDooperWorld!


# Cloud repositories

As of 2017, there are still people developing code on their local machine. 
Do these people make backups in a USB? Do they share their code with coworkers via email or Whatsapp? 
Having your code in the cloud has many advantages, it is generally more secure, especially if it is hosted on 
GitHub where they have professional security. It is easier to work collaboratively, any team member can download 
the latest version of the repository from any machine. It is cheaper than a traditional server; in fact, GitHub 
allows to host open-source projects for free and BitBucket allows to do the same for private repos.


Here is some new line!

# Distributed file system

Git is distributed, meaning that every local copy of the global repository is a fully working copy. 
In case there is a problem with the server and the global repository is corrupted or lost, any local 
copy can recreate the full history. This file system architecture is the opposite of centralized file 
systems like Subversion (SVN), very popular before GitHub. In a centralized version control system, 
the global server contains all changes in the project and the local copies are just light versions it.
If the server goes down, you lose all the history.


How do you spell sooper?

# Perfect to work with others

Git is designed to creating projects where many contributors develop software in parallel. 
Specifically, it has a very powerful way to resolve conflicts when two people are working on the same file. 
Its stability is much higher than SVN. Those who have used SVN knows what I'm talking about.


# Good documentation

Git has been around for many years now and it's really easy to find good documentation. 
I have my own tutorial, where I explain the basics of git and add useful commands that I use from time to time.


# Easy to use

Nobody can say git is difficult. To do the day-to-day work, you just need to manage these 8 commands: 
git clone, git status, git add, git commit, git push, git pull, git checkout and git branch. 
Their explanation can be found here. With them you can download a repo, check its status, 
add files, send files to the server, retrieve the latest changes on the server and 
create branches to work in parallel.


# Branches allows for simultaneous code versions

Branches are one of the best features of a version control software and are used to develop in parallel 
to the main repository. A branch is a fork of the main code to develop a new feature. When you create 
a new functionality in the code, you should create a branch. Then you develop the functionality, 
test it and when it works perfectly, you can integrate it or merge into the main branch 
(which is usually the master branch). When using branches, you can have simultaneous 
versions of the same code.


# Encourages code reviews

Code reviews are a good practice that every developer team should follow. Git facilitates code reviews
with an operation called pull request. A pull request is a merge operation between two branches, one of 
them is typically the master branch. The standard workflow is the following: a developer works in a branch 
coding a new feature, when the feature is finished a pull request is submitted. A different developer reviews 
the code of the pull request and suggests improvements. Once the improvements are made, the branch is merged 
into master, and thus a new feature is added to the project.


# It's easy to revert your mistakes

Every time you send code to the server you make a commit. Every commit is referenced with a hash that uniquely 
identifies it, see this example. With git, it is super easy to revert to any past commit and fix a mistake.


# It's the current de facto open-source umbrella

If you want to develop open-source code, the biggest repository is GitHub. Here you can find the most popular 
repositories in Github, it includes bootstrap, react, d3, tensorflow, angular, etc.


# Connectivity to cloud services

Git popularity generated the emergence of other services that complete the software development stack, 
sometimes called git hooks or git integrations. An interesting case is continuous integration systems 
like Travis. These systems allow for automatic testing of a software solution. Another useful tool is 
Waffle. This system helps developers to plan and track a project linked directly with GitHub. 
Finally, another interesting tool is Codacy that generates automatic code reviews.


# Source 
https://miguelgfierro.com/blog/2017/10-reasons-why-you-should-be-using-git-in-software-projects/
